# Destinations

- North America
  - USA
    - Denali National Park & Preserve, Alaska
    - Redwood National and State Parks, California
    - [Golden Gate Bridge](north-america/usa/GoldenGateBridge.md), California
    - [Alcatraz Island](north-america/usa/AlcatrazIsland.md), California
    - Big Sur, California
    - [Griffith Observatory](north-america/usa/GriffithObservatory.md), California
    - Yosemite National Park, California
    - Death Valley National Park, California
    - Grand Canyon National Park, Arizona
    - Monument Valley, Arizona
    - Yellowstone National Park, Wyoming
    - Rocky Mountain National Park, Colorado
    - Mount Rushmore National Memorial, South Dakota
    - Graceland, Tennessee
    - Art Institute of Chicago, Illinois
    - Walt Disney World, Florida
    - Everglades National Park, Florida
    - Blue Ridge Parkway, Virginia
    - Lincoln Memorial, Washington DC
    - National Mall, Washington DC
    - Independence National Historical Park, Pennsylvania
    - Empire State of Building, New York
    - Metropolitan Museum of Art, New York
    - Statue of Liberty & Ellis Island, New York
    - Times Square, New York
    - National September 11 Memorial & Museum, New York
    - Cape Cod National Seashore, Massachusetts

## Resources

- Lonely Planet
- DK Eyewitness Travel Guides
- Frommer's
- Hunter Travel Guides
- Rough Guides
