# Alcatraz Island

## In Popular Culture

### Movies

- Escape from Alcatraz (1979): [Amazon Prime](https://www.amazon.com/dp/B000IYXVM2)
- Birdman of Alcatraz (1962)

## When to Visit

To avoid crowds, schedule your visit during the winter months of January through March. To experience the nicest weather, plan your Alcatraz visit for April-May or September-October. (Surprisingly, summer actually brings cold and foggy weather to the island.)

## Tickets

Access to Alcatraz Island is by commercial ferry service - buy tickets at [Alcatraz Cruises](http://www.alcatrazcruises.com/website/buy-tickets.aspx). Tickets go on sale 90 days in advance.

## Weather

The climate on Alcatraz is unpredictable and can change suddenly. Cold, foggy mornings may give way to sunny afternoons, which in turn can shift quickly back to more fog and blustery winds.

The most pleasant weather usually occurs during April, May and June, and then again during September and October. Summer brings some of the coolest weather and heaviest fog to the island, which surprises many out-of-town visitors to San Francisco.

Temperatures on Alcatraz seldom rise above 75°F (24°C) or fall below 38°F (3°C). It almost never snows on the island, but winters are wet and always cold. Afternoon winds are common during every season.

Rain occurs frequently during winter and early spring. Much of the Alcatraz tour route is along outside roadways without shelter from the rain, so dress for possible wet weather. (Rain gear is available for purchase at Alcatraz Landing and in the island’s bookstores.)

Native San Franciscans have a saying: "Always dress in layers and hope to be pleasantly surprised."

## What to Wear

The weather on Alcatraz is unpredictable and subject to change unexpectedly, so be sure to bring along a light jacket or sweater no matter how nice the day starts out. The best advice is always to dress in layers.

Sunglasses and sun block are recommended, since sunlight reflecting off the bay’s surface can be dangerously bright even on a cold, windy day.

Rain is common during the winter months. Since much of the Alcatraz tour route follows outside roads without overhead cover, be sure to bring rain gear. (Rain wear is available for sale at Alcatraz Landing and in the bookstores on the island.)

The roads on Alcatraz are steep, so wear comfortable walking shoes with grip-type soles.

What NOT to wear: Sandals, leather-soled shoes, high heels, and open-toe shoes.

Keep your possessions with you at all times. No lockers are provided on Alcatraz for storing visitors’ items. Avoid bringing large objects to the island such as oversized backpacks, luggage or bikes.

NOTE: Backpacks, handbags, parcels and packages are subject to search at any time for security purposes.

## Directions

The Alcatraz Ferry Terminal is located on The Embarcadero near the intersection of Bay Street at Pier 33.

## Public Transportation

A handy way to organize your trip to Alcatraz Landing is to visit the [511 TakeTransit Trip Planner](http://transit.511.org/).

## Parking

### Commercial Parking Lots

There are 15 commercial lots with a five-block radius of Alcatraz Landing at Pier 33, with a total of more than 3,000 parking spaces. The closest and most convenient lot is at 80 Francisco at Kearny, just one block away, across the Embarcadero from Alcatraz Landing. Strongly recommend using public transit to get to Pier 33, which is on the Embarcadero south of the intersection of Bay Street. The historic streetcars of the MUNI F Line run right past the Alcatraz Landing. See transit.511.org for more information on public transportation.

### Accessible Parking

Alcatraz Landing at Pier 33 in San Francisco is fully accessible. There is limited accessible parking at Pier 33, available on a first-come, first-served basis. Individuals need to show their permanent or temporary disabled placard at the entry of Pier 33 to utilize the accessible parking spaces.

### On-Street Parking

On-street parking in the Fisherman's Wharf area can be difficult to find, especially during peak summer visitation season, and nearly every street space has a parking meter. Visitors should also be aware that meters are restricted to four hours maximum parking. (A visit to Alcatraz and back takes approximately 2.5 hours.)

## References

- [National Park Service](https://www.nps.gov/alca/index.htm)
- [Alcatraz Cruises](http://www.alcatrazcruises.com)
- [Lonely Planet](https://www.lonelyplanet.com/usa/san-francisco/attractions/alcatraz/a/poi-sig/383435/361858)
- [Travel + Leisure](http://www.travelandleisure.com/travel-guide/fishermans-wharf/things-to-do/alcatraz-island)
- [Trip Advisor](https://www.tripadvisor.com/Attraction_Review-g60713-d102523-Reviews-Alcatraz_Island-San_Francisco_California.html)
