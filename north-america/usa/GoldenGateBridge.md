# Golden Gate Bridge

## When to Visit

The best months to visit can often be May/June and September/October.

## Directions

[Directions](http://goldengatebridge.org/visitors/directions.php)

## Bike Rentals

Electric bikes or small scooters may NOT be ridden across the sidewalk in the power-on mode. They are allowed to be pushed or ridden across the sidewalk in the power-off mode only.

[Bike Rentals](https://www.yelp.com/search?find_desc=Bike+Rentals&find_loc=Fisherman%27s+Wharf,+San+Francisco,+CA)

## Weather

The climate is temperate marine and generally mild year-round. Daytime temperatures range from 40 degrees in the winter to 75 degrees in the summer. Morning and evening fog rolls in during the summer and winter months, and can burn off by midday.

## Resources

- [Golden Gate Bridge Highway & Transportation District](http://goldengatebridge.org/visitors/)
